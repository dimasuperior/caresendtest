import { parse, format } from "date-fns";

export function formatDate(dateString: string): string {
  const date = parse(dateString, "MM.dd", new Date());
  return format(date, "MMMM, do");
}

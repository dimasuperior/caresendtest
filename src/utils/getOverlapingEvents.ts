import { EventView } from "@/model/event";
import { sortBy } from "lodash-es";

type Point = {
  value: number;
  type: "start" | "end";
  eventId: string;
};

/**
 * Idea for algorithm for finding overlapping events:
 *
 * Returns 'map' of eventID to array of overlapping eventIDs. E.g.
 * {
 *     'e01' -> ['e03', 'e04'],
 *     'e03' -> ['e01'],
 *     'e04' -> ['e01'],
 * }
 *
 * An event is defined by two points (start and end). All points are put in the array ("timeline"), then sort array and
 * step through array taking a point. (Sort costs O(NLogN) )
 * If point has type 'start' that means an event is started, and point having type = 'end' means event is finished.
 * Also we need additional data structure (set, name "current events") which contains events (ids) that are started and not finished yet.
 *
 *
 * Iterate over the array ("timeline") of points. (2N operations * M - max size of "current events" set)
 *   - If point has type = 'end'
 *     - remove point's eventID from "current events" set
 *   - If point has type = 'start'
 *      - If current events has anything:
 *          1. Iterate over "current events" set getting a key, // it cost M - size of current events set, in worst case M = N
 *              then add point's eventID to the value of result[key]
 *          2. Add point's eventID as key to result map and set all of "current event" keys as value // it also costs M operations
 *
 * Return result map
 */

export function getIntersectingEventsMap(
  events: EventView[]
): Record<string, string[]> {
  const result: Record<string, string[]> = {};
  const points: Point[] = [];
  events.forEach((event) => {
    points.push({
      eventId: event.id,
      value: event.start,
      type: "start",
    });
    points.push({
      eventId: event.id,
      value: event.end,
      type: "end",
    });
  }); // O(N)
  const sortedPoints = sortBy(points, "value", "type"); // O(NlogN) - it uses Array.prototype.sort, which implements Timsort in V8

  // It takes additional N
  const currentEvents = new Set<string>();
  // SortedPoints array has 2N of elements
  // Inner loop takes M = size of "current events"
  // In worst case M = N
  // So this loop takes 0(N*M)
  for (const point of sortedPoints) {
    if (point.type === "end") {
      currentEvents.delete(point.eventId);
    }
    if (point.type === "start") {
      if (currentEvents.size !== 0) {
        // this costs M = size of current events
        for (const eventID of currentEvents.keys()) {
          if (Array.isArray(result[eventID])) {
            result[eventID].push(point.eventId);
          } else {
            result[eventID] = [point.eventId];
          }
        }
        // this costs M = size of current events
        result[point.eventId] = Array.from(currentEvents.keys());
      }
      currentEvents.add(point.eventId);
    }
  }

  return result;
}

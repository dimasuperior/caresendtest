import Vue from "vue";
import Vuex from "vuex";
import { Event, EventDTO, EventView } from "@/model/event";
import { sortBy } from "lodash-es";
import { getIntersectingEventsMap } from "@/utils/getOverlapingEvents";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    events: [],
  },
  mutations: {
    setEventList(state, events) {
      state.events = events;
    },
  },
  actions: {
    async fetchEventList({ commit }) {
      const response = await fetch(
        "https://mocki.io/v1/058be9e6-3a07-4e8d-a14a-d354a3b953a4"
      );
      let result: EventView[] = [];
      if (response.ok) {
        const eventDTOs: EventDTO[] = await response.json();
        if (Array.isArray(eventDTOs)) {
          result = eventDTOs.map(Event.DTOtoView);
          result = sortBy(result, ["start", "end"]);
        }
      }

      commit("setEventList", result);
    },
  },
  getters: {
    eventsGroupByDate: (state) => {
      const result: Record<string, unknown[]> = {};
      state.events.forEach((evt: EventView) => {
        const groupKey = `${evt.startMonth}.${evt.startDay}`;
        if (result[groupKey]) {
          result[groupKey].push(evt);
        } else {
          result[groupKey] = [evt];
        }
      });
      return Object.entries(result);
    },
    overlappedEvents: (state) => {
      return getIntersectingEventsMap(state.events);
    },
  },
  modules: {},
});

import { uniqueId } from "lodash-es";

export interface EventDTO {
  title: string;
  start: string;
  end: string;
}

export interface EventView {
  id: string;
  title: string;

  start: number;
  startDay: string;
  startMonth: string;
  startHours: string;
  startMinutes: string;

  end: number;
  endDay: string;
  endMonth: string;
  endHours: string;
  endMinutes: string;
}

export class Event {
  static DTOtoView(dto: EventDTO): EventView {
    // assuming dto.start and dto.end are correct
    const startDate = new Date(dto.start);
    const endDate = new Date(dto.end);
    return {
      id: uniqueId(),
      title: dto.title,
      start: Date.parse(dto.start),
      startMonth: `${startDate.getMonth()}`.padStart(2, "0"),
      startDay: `${startDate.getDate()}`.padStart(2, "0"),
      startHours: `${startDate.getHours()}`.padStart(2, "0"),
      startMinutes: `${startDate.getMinutes()}`.padStart(2, "0"),
      end: Date.parse(dto.end),
      endMonth: `${endDate.getMonth()}`.padStart(2, "0"),
      endDay: `${endDate.getDate()}`.padStart(2, "0"),
      endHours: `${endDate.getHours()}`.padStart(2, "0"),
      endMinutes: `${endDate.getMinutes()}`.padStart(2, "0"),
    };
  }
}

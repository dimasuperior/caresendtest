import { shallowMount } from "@vue/test-utils";
import EventList from "@/components/EventList.vue";

describe("EventList.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(EventList, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
